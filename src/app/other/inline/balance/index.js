/**
 * User's current balance
 */
import { Op } from 'sequelize'
import dayjs from 'dayjs'
import 'dayjs/locale/ru.js'
import utc from 'dayjs/plugin/utc.js'
import duration from 'dayjs/plugin/duration.js'

dayjs.locale('ru')
dayjs.extend(utc)
dayjs.extend(duration)

import { Score } from '#models/score.js'
import { SCORE_TYPES } from '#models/scoretype.js'
import { numberToWords } from '#utils/helpers.js'
import { getAuth } from '#controllers/userController.js'

/**
 * Total score icon based on score value
 */
function getScoreIcon(value) {
    if (value >= 5000) return '☠️'
    else if (value >= 3000) return '💩'
    else if (value >= 2000) return '🤯'
    else if (value >= 1000) return '😭'
    else if (value > 0) return '😔'
    else return '🦾'
}

/**
 * Sweet icon based on free sweets
 */
function getSweetIcon(value) {
    if (value > 2) return '🍰'

    switch(value) {
        case 0: return '🚫'
        case 1: return '🍦'
        case 2: return '🍮'
    }
}

/**
 * Number to word
 */
function durationToWords(duration) {
    const time = []

    if (duration.days() > 0) {
        time.push(
            duration.days() + ' ' +
            numberToWords(duration.days(), 'день_дня_дней')
        )
    }

    if (duration.hours() > 0) {
        time.push(
            duration.hours() + ' ' +
            numberToWords(duration.hours(), 'час_часа_часов') + ' и'
        )
    }

    time.push(
        duration.minutes() + ' ' +
        numberToWords(duration.minutes(), 'минуту_минуты_минут')
    )

    return time.join(' ')
}

export default async function(ctx) {
    const user = await getAuth(ctx)

    const scoreIcon = getScoreIcon(+user.total_score)
    const sweetIcon = getSweetIcon(+user.sweets)

    const rows = [
        `${user.linkName()} - <b>Баланс</b>\n\n`,
        `${scoreIcon} Текущий счет ${user.total_score}\n`,
        `${sweetIcon} Бесплатных сладостей ${user.sweets}`,
    ]

    const score = await Score.findOne({
        where: {
            user_id: user.id,
            type_id: { [Op.not]: SCORE_TYPES.FREE_SWEET }
        },
        order: [ [ 'created_at', 'desc' ] ],
    })

    if (score) {
        const createdAt = dayjs(score.created_at).add(7, 'days')

        if (dayjs().isBefore(createdAt) &&
            (+user.total_score === 0 || +user.current_score === 0)) {

            const difference = createdAt.diff(dayjs())

            const duration = dayjs.duration(difference)

            rows.push(`\n⏱ Получит сладость через ${durationToWords(duration)}`)
        }
        else if (user.sweet_score_id === score.id) {
            const sweetAddedAt = dayjs(score.created_at)
                .utcOffset(user.utc)
                .add(7, 'day')
                .format('D MMMM')

            rows.push(`\n😋 Получена сладость ${sweetAddedAt}`)
        }
    }

    return ctx.editMessageText(rows.join(''), { parse_mode: 'HTML' })
}
