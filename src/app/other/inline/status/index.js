/**
 * Basic user's information
 */
import { sequelize } from '#connection'
import { getAuth } from '#controllers/userController.js'

const TOTAL_USERS_SCORE =
`select type_id, SUM(value) as total
from score
where user_id=?
group by user_id, type_id;`

export default async function(ctx) {
    const user = await getAuth(ctx)

    const [ result ] = await sequelize.query(TOTAL_USERS_SCORE, {
        replacements: [ user.id ],
    })

    const status = result.reduce((red, elem) => {
        red[elem.type_id] = elem.total

        return red
    }, {})

    return ctx.editMessageText([
            `${user.linkName()} - <b>Статус</b>\n\n`,
            `<b>${status.R || '🚫'}</b> : бесплатных 🎂\n\n`,
            `<b>${status.P || '🚫'}</b> : отжимания 🦾\n`,
            `<b>${status.A || '🚫'}</b> : пресс 🥦\n`,
            `<b>${status.B || '🚫'}</b> : спина 🍖\n`,
            `<b>${status.Q || '🚫'}</b> : приседания 🦿\n\n`,
            `<b>${status.S || '🚫'}</b> : сладкое 🥞\n`,
            `<b>${status.O || '🚫'}</b> : переедания 🍱\n`,
            `<b>${status.C || '🚫'}</b> : на спор 🥊\n`,
            `<b>${status.F || '🚫'}</b> : штрафа 🔫\n`,
        ].join(''), { parse_mode: 'HTML' })
}
