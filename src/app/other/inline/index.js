/**
 * Inline query mode
 */
import { Markup } from 'telegraf'

/** Seconds to cache inline query */
const CACHE_TIME = 300

/** Actions (action|text) */
const actions = [
    'balance|🤸 Баланс',
    'status|📝 Статус',
    'activity|📖 История',
]

/**
 * Inline query method
 */
async function inlineQuery(ctx) {
    const buttons = actions.map(elem => {
        const [ _action, _text ] = elem.split('|')

        return [ Markup.button.callback(_text, `inline-${_action}`) ]
    })

    return ctx.answerInlineQuery([
        {
            id: 'inline-lfbw',
            type: 'article',
            title: 'ЛЕТО - основное меню',
            input_message_content: {
                message_text: 'Список действий',
            },
            ...Markup.inlineKeyboard(buttons)
        }
    ], {
        cache_time: CACHE_TIME
    })
}

/**
 * Setting inline mode
 */
export default async function(bot) {
    bot.on('inline_query', inlineQuery)

    for (let action of actions) {
        const [ _action ] = action.split('|')

        const module = await import(`./${_action}/index.js`)

        bot.action(`inline-${_action}`, module.default)
    }

    return bot
}
