/**
 * User's activity for the last week
 */
import { Op } from 'sequelize'
import dayjs from 'dayjs'
import 'dayjs/locale/ru.js'
import utc from 'dayjs/plugin/utc.js'

dayjs.locale('ru')
dayjs.extend(utc)

import { Score } from '#models/score.js'
import { SCORE_TYPES } from '#models/scoretype.js'
import { getAuth } from '#controllers/userController.js'

/** Amount of activity rows */
const SCORES_AMOUNT = 20

export default async function(ctx) {
    const user = await getAuth(ctx)

    const scores = await Score.findAll({
        where: {
            user_id: user.id,
            created_at: {
                [Op.gt]: new Date(new Date() - 7 * 24 * 60 * 60000),
            },
        },
        order: [ [ 'created_at', 'desc' ] ],
        include: [ 'Scoretype' ],
        limit: SCORES_AMOUNT,
    })

    const rows = [
        `${user.linkName()} - <b>История</b>\n\n`,
    ]

    if (scores.length !== 0) {
        const { PUSH_UP, ABDOMINAL, BACK, SQUAT } = SCORE_TYPES

        let sign = ''

        for (let score of scores) {
            sign = '+'

            const createdAt = dayjs(score.created_at)
                .utcOffset(user.utc)
                .format('D MMM HH:mm:ss')

            if ([ PUSH_UP, ABDOMINAL, BACK, SQUAT ].includes(score.type_id) ||
                score.value < 0) {
                sign = '-'
            }

            rows.push(`<i>${createdAt}</i>: <b>${sign}${score.value}</b> ${score.Scoretype.about}\n`)
        }
    }
    else {
        rows.push('<i>Нет активности за неделю</i>')
    }

    return ctx.editMessageText(rows.join(''), { parse_mode: 'HTML' })
}
