/**
 * Academia master class
 */
import fs from 'fs'
import path from 'path'
import axios from 'axios'
import * as https from 'https'
import schedule from 'node-schedule'
import dayjs from 'dayjs'
import 'dayjs/locale/ru.js'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)
dayjs.locale('ru')

import { numberToWords } from '#utils/helpers.js'

let _bot = null

/** URL to get data to upcoming master class */
const URL_INFO = 'https://akademiacoffee.com/masterclass-data.fn'

/** Main information page of mater class */
const URL_MAIN = 'https://akademiacoffee.com/ru/lessons/'

/** Path to cache of meta data */
const CACHE_PATH = path.resolve('./src/storage/app/academia.json')

/** Users to notify about event */
const USER_IDS = [
    501244780,
    1211871972,
]

async function masterClass() {
    const day = dayjs().utcOffset(7).day()
    const hour = dayjs().utcOffset(7).hour()

    /** Do not check out during the night */
    if (hour >= 0 && hour < 8 ||
        day === 0 || day === 6) return

    let cache = {}

    try {
        cache = JSON.parse(
            fs.readFileSync(CACHE_PATH, { encoding: 'utf-8' })
        )
    } catch {  }

    let startAt = dayjs(cache.date || dayjs().subtract(7, 'day'))
    let daysLeft = dayjs().diff(startAt, 'days')

    /** Event passed and since then should check it out again */
    if (cache.notified && daysLeft >= 0) {
        cache.notified = false

        fs.writeFileSync(CACHE_PATH, JSON.stringify(cache))
    }

    /** Assume event will start in less then a week and no users notified */
    if (! cache.notified && daysLeft >= 7) {
        try {
            const httpsAgent = new https.Agent({ rejectUnauthorized: false })
            const { data } = await axios.post(URL_INFO, {}, { httpsAgent })

            cache = data.find(elem => elem.city.ru === 'Красноярск')
        } catch {  }

        if (cache?.date && ! dayjs(cache.date).isSame(startAt)) {
            startAt = dayjs(cache.date)
            daysLeft = dayjs().diff(startAt, 'days')

            cache.notified = false

            fs.writeFileSync(CACHE_PATH, JSON.stringify(cache))
        }
    }

    /** Event will be started soon and no users notified */
    if (! cache.notified && daysLeft < 0) {
        let eventLink = `<a href="${URL_MAIN}">мастер-класс</a>`
        let eventTimes = ''

        if (cache.list.length > 0) {
            let _times = cache.list.map(elem => elem.time)

            if (_times.length > 1) {
                const _lastTime = _times.pop()

                _times = _times.join(', ') + ' и ' + _lastTime
            }
            else {
                _times = _times[0]
            }

            const eventPlaces = numberToWords(cache.places, 'место_места_мест')

            eventTimes =
                `\nНа ${_times} ` +
                `всего осталось ${cache.places} ${eventPlaces}.`
        }

        const eventMessage =
            '<b>Академия кофе ☕️</b>\n\n' +
            `Новый ${eventLink} ${startAt.format('DD MMMM')},\n` +
            `на тему <i>«${cache.header.ru}»</i>.\n` +
            eventTimes

        for (let userId of USER_IDS) {
            await _bot.telegram.sendMessage(
                userId,
                eventMessage,
                {
                    parse_mode: 'HTML',
                    disable_web_page_preview: true,
                }
            )
                .catch(error =>
                    console.log(
                        'Error to send academia message' +
                        `to user #${userId} - ${error.message}`
                    )
                )
        }

        cache.notified = true

        fs.writeFileSync(CACHE_PATH, JSON.stringify(cache))
    }
}

export default function(bot) {
    if (!bot) return null

    _bot = bot

    return schedule.scheduleJob('*/15 * * * *', masterClass)
}
