/**
 * Add sweets after a week without fines
 */
import schedule from 'node-schedule'
import { sequelize } from '#connection'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

import { User } from '#models/user.js'
import { Score } from '#models/score.js'
import { SCORE_TYPES } from '#models/scoretype.js'

let _bot = null

const ZERO_SWEETS_QUERY =
`SELECT score.* FROM score
JOIN (
  SELECT id, MAX(created_at)
  FROM score
  WHERE type_id NOT IN ('${SCORE_TYPES.FREE_SWEET}')
  GROUP BY user_id
) _score ON score.id=_score.id
join user on score.user_id=user.id
WHERE user.total_score=0
  and score.id<>user.sweet_score_id;`

async function automatedSweetsAddition() {
    const [ scores ] = await sequelize.query(ZERO_SWEETS_QUERY)

    for (let score of scores) {
        if (dayjs().diff(score.created_at, 'week') < 1) continue

        const user = await User.findByPk(score.user_id)

        await Score.create({
            user_id: user.id,
            type_id: SCORE_TYPES.FREE_SWEET,
            value: 1,
            current_score: user.total_score,
            created_at: dayjs(score.created_at).add(7, 'day')
        })

        user.sweets = +user.sweets + 1
        user.sweet_score_id = score.id

        await user.save()

        if (! Boolean(user.notify('free'))) continue

        await _bot.telegram.sendMessage(
            user.id,
            'Целая неделя без штрафов 🦾\n' +
            'Вам начислена бесплатная сладость 😋'
        )
            .catch(error =>
                console.log(
                    'Error to send sweets message ' +
                    `to user #${user.id} - ${error.message}`
                )
            )
    }
}

/**
 * Sweets addition every two hours
 */
export default function(bot) {
    if (!bot) return null

    _bot = bot

    return schedule.scheduleJob('0 */4 * * *', automatedSweetsAddition)
}
