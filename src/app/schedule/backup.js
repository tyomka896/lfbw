/**
 * Backup of database
 */
import fs from 'fs'
import path from 'path'
import schedule from 'node-schedule'
import dayjs from 'dayjs'

const DATABASE_PATH = path.resolve('./src/storage/app/db.sqlite')

/**
 * Format of backup file name
 */
function fileBackupName() {
    return `db.${dayjs().format('YYYYMMDD')}.sqlite`
}

/**
 * Backup database file to google drive
 */
async function backupSQLiteDatabase() {
    if (!fs.existsSync(DATABASE_PATH)) {
        console.error('Backup has canceled, SQLite file was not found!')

        return
    }

    const BACKUP_PATH = path.resolve('../backup', fileBackupName())

    fs.copyFile(DATABASE_PATH, BACKUP_PATH, error => {
        if (error) {
            console.error('Local backup ERROR:', error.message)
        }
    })
}

/** Backup every Monday at 00:00 */
export default function() {
    return schedule.scheduleJob('0 0 * * 1', backupSQLiteDatabase)
}
