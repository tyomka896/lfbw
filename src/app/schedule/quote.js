/**
 * Everyday quote
 */
import schedule from 'node-schedule'
import { Op } from 'sequelize'
import axios from 'axios'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

import { User } from '#models/user.js'
import { random } from '#utils/random.js'

let _bot = null

/** Exact hour time to sent a message */
const TARGET_HOUR = 10

/**
 * Get hour offset based on needed scheduled hour
 */
function getHourOffset(targetHour) {
    const _offset = targetHour - dayjs().utc().hour()

    if (_offset <= -12) return _offset + 24
    else if (_offset > 12) return _offset - 24

    return _offset
}

/**
 * Get random emoji
 */
function getRandomEmoji() {
    const emojis = [
        '😀', '😊', '😜', '🧐', '🤓', '😏',
        '🤗', '🤔', '🤭', '🙄', '😯', '👻',
    ]

    return random(emojis)
}

/**
 * Get quote of the day
 */
async function getQuoteOfTheDay() {
    let quote = null

    try {
        const { data } = await axios('https://zenquotes.io/api/today')
        quote = data.pop()
    } catch (error) {
        return null
    }

    return [
        `<b>Quote of the day</b> ${getRandomEmoji()}\n\n`,
        `«${quote.q.trim()}» - <i>${quote.a}</i>`,
    ].join('')
}

/**
 * Everyday quote for every workday
 */
async function everydayQuoteSchedule() {
    const hourOffset = getHourOffset(TARGET_HOUR)

    const weekDay = dayjs.utc().add(hourOffset, 'hour').day()

    /** Except Saturday or Sunday */
    if (weekDay === 6 || weekDay === 0) return

    const users = (await User.findAll({
        where: {
            [Op.and]: [ { utc: hourOffset } ]
        }
    }))
        .filter(elem => Boolean(elem.notify('quote')))

    if (users.length === 0) return

    const message = await getQuoteOfTheDay()

    if (!message) return

    for (let user of users) {
        await _bot.telegram.sendMessage(
            user.id,
            message,
            { parse_mode: 'HTML' }
        )
            .catch(error =>
                console.log(
                    'Error to send quote message ' +
                    `to user #${user.id} - ${error.message}`
                )
            )
    }
}

/**
 * Schedule for everyday quote
 */
export default function(bot) {
    if (!bot) return null

    _bot = bot

    return schedule.scheduleJob('0 * * * *', everydayQuoteSchedule)
}
