/**
 * Penalty addition at the end of the week
 * on non zero user's total score
 */
import schedule from 'node-schedule'
import { Op } from 'sequelize'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

import { User } from '#models/user.js'
import { Score } from '#models/score.js'
import { SCORE_TYPES } from '#models/scoretype.js'
import { fineByLevel, calcWeekFine } from '#utils/helpers.js'

let _bot = null

/**
 * Week penalty addition for users
 */
async function automatedWeekPenaltyAddition() {
    const weekDay = dayjs().utc().day()

    /** Only Sunday and Monday */
    if (weekDay !== 1 && weekDay !== 0) return

    let hourOffset = dayjs().utc().hour()

    if (hourOffset !== 0) {
        /** From UTC-12:00 to UTC+00:00 on Monday */
        if (weekDay === 1) {
            hourOffset = -hourOffset
        }
        /** From UTC+00:00 to UTC+12:00 on Sunday */
        else if (weekDay === 0) {
            hourOffset = 24 - hourOffset
        }
    }

    const users = await User.findAll({
        where: {
            [Op.and]: [
                { total_score: { [Op.not]: 0 } },
                { utc: hourOffset },
            ]
        }
    })

    if (users.length === 0) return

    for (let user of users) {
        /** Ignore anyone who has no score activity in a week */
        const scoreNotIn = [
            SCORE_TYPES.FINE,
            SCORE_TYPES.FREE_SWEET,
        ]

        const lastScore = await Score.findOne({
            where: {
                [Op.and]: [
                    { user_id: user.id },
                    { type_id: { [Op.notIn]: scoreNotIn } },
                ],
            },
            order: [ [ 'created_at', 'desc' ] ],
        })

        if (
            lastScore &&
            dayjs().diff(dayjs(lastScore.created_at), 'days') >= 7
        ) continue

        /** Max score and fine per week */
        const maxFine = fineByLevel(user.level, user.week_fine)

        let totalScore = +user.total_score

        if (totalScore < maxFine) {
            totalScore = calcWeekFine(totalScore, user.week_fine)

            if (totalScore > maxFine) totalScore = maxFine
        }

        /** No need to add fine to user */
        if (user.total_score === totalScore) continue

        const scoresAdded = totalScore - user.total_score

        await Score.create({
            user_id: user.id,
            type_id: SCORE_TYPES.FINE,
            value: scoresAdded,
            current_score: totalScore,
        })

        user.total_score = totalScore

        await user.save()

        if (Boolean(user.notify('fine'))) {
            await _bot.telegram.sendMessage(
                user.id,
                `Вам добавлен недельный штраф +${scoresAdded} за не отработанный счет 😜`
            )
                .catch(error =>
                    console.log(
                        'Error to send fine message ' +
                        `to user #${user.id} - ${error.message}`
                    )
                )
        }
    }
}

/**
 * Week fine addition on every timezone from -12:00 to +12:00
 */
export default function(bot) {
    if (!bot) return null

    _bot = bot

    return [
        /** From UTC+00:00 to UTC+12:00 on Sunday */
        schedule.scheduleJob('0 12-23 * * 7', automatedWeekPenaltyAddition),
        /** From UTC-12:00 to UTC+00:00 on Monday */
        schedule.scheduleJob('0 0-12 * * 1', automatedWeekPenaltyAddition),
    ]
}
