/**
 * Overeating edit
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { getAuth } from '#controllers/userController.js'

/** Keyboard */
export async function notifyKeyboard(ctx) {
    const auth = await getAuth(ctx)

    const [ fine, free, quote ] = [
        auth.notify('fine') ? '✓' : '✗',
        auth.notify('free') ? '✓' : '✗',
        auth.notify('quote') ? '✓' : '✗',
    ]

    return [
        'Уведомления',
        Markup.inlineKeyboard([
            [ Markup.button.callback(`${fine} Штрафы`, 'settings-notify-fine') ],
            [ Markup.button.callback(`${free} Бесплатности`, 'settings-notify-free') ],
            [ Markup.button.callback(`${quote} Цитаты`, 'settings-notify-quote') ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-settings') ],
        ])
    ]
}

/** Actions */
mainScene.action(/settings-notify-(fine|free|quote)/, async ctx => {
    const value = ctx.match[0].replace('settings-notify-', '')

    if (!value) return

    const auth = await getAuth(ctx)

    auth.notify({ [value]: !Boolean(auth.notify(value)) })

    return ctx.editMessageText(...await notifyKeyboard(ctx))
})
