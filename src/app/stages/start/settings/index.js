/**
 * Settings
 */
import { Markup } from 'telegraf'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

import { mainScene } from '#scene'
import { sweetKeyboard } from './sweet.js'
import { overeatingKeyboard } from './overeating.js'
import { weekKeyboard } from './week.js'
import { levelKeyboard } from './level.js'
import { utcKeyboard } from './utc.js'
import { notifyKeyboard } from './notify.js'
import { getAuth } from '#controllers/userController.js'

/** Keyboard */
export async function settingsKeyboard(ctx) {
    const auth = await getAuth(ctx)

    return [
        'Настройки',
        Markup.inlineKeyboard([
            [ Markup.button.callback(`🥞 Сладость (${auth.sweet_fine})`, 'settings-sweet') ],
            [ Markup.button.callback(`🍱 Переедание (${auth.overeating_fine})`, 'settings-overeating') ],
            [ Markup.button.callback(`🔫 Недельный штраф (${auth.week_fine}%)`, 'settings-week') ],
            [ Markup.button.callback(`🪁 Уровень (${auth.level})`, 'settings-level') ],
            [ Markup.button.callback(`🧭 Регион (${dayjs().utcOffset(auth.utc).format('Z')})`, 'settings-utc') ],
            [ Markup.button.callback(`📨 Уведомления`, 'settings-notify') ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-start') ],
        ])
    ]
}

/** Actions */
mainScene.action('settings-sweet', async ctx => {
    await ctx.answerCbQuery()

    return ctx.reply(...await sweetKeyboard(ctx))
})

mainScene.action('settings-overeating', async ctx => {
    await ctx.answerCbQuery()

    return ctx.reply(...await overeatingKeyboard(ctx))
})

mainScene.action('settings-week', async ctx => {
    return ctx.editMessageText(...await weekKeyboard(ctx))
})

mainScene.action('settings-level', async ctx => {
    await ctx.answerCbQuery()

    return ctx.reply(...await levelKeyboard(ctx))
})

mainScene.action('settings-utc', async ctx => {
    return ctx.editMessageText(...await utcKeyboard(ctx))
})

mainScene.action('settings-notify', async ctx => {
    return ctx.editMessageText(...await notifyKeyboard(ctx))
})

mainScene.action('back-to-settings', async ctx => {
    return ctx.editMessageText(...await settingsKeyboard(ctx))
})
