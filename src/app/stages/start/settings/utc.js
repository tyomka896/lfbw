/**
 * UTC
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { settingsKeyboard } from './index.js'
import { getAuth } from '#controllers/userController.js'

/** Keyboard */
export async function utcKeyboard() {
    return [
        'Выберите регион:',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('-12', 'settings-utc-12'),
                Markup.button.callback('-11', 'settings-utc--11'),
                Markup.button.callback('-10', 'settings-utc--10'),
                Markup.button.callback('-9', 'settings-utc--9'),
                Markup.button.callback('-8', 'settings-utc--8'),
            ],
            [
                Markup.button.callback('-7', 'settings-utc--7'),
                Markup.button.callback('-6', 'settings-utc--6'),
                Markup.button.callback('-5', 'settings-utc--5'),
                Markup.button.callback('-4', 'settings-utc--4'),
                Markup.button.callback('-3', 'settings-utc--3'),
            ],
            [
                Markup.button.callback('-2', 'settings-utc--2'),
                Markup.button.callback('-1', 'settings-utc--1'),
                Markup.button.callback('+0', 'settings-utc-0'),
                Markup.button.callback('+1', 'settings-utc-1'),
                Markup.button.callback('+2', 'settings-utc-2'),
            ],
            [
                Markup.button.callback('+3', 'settings-utc-3'),
                Markup.button.callback('+4', 'settings-utc-4'),
                Markup.button.callback('+5', 'settings-utc-5'),
                Markup.button.callback('+6', 'settings-utc-6'),
                Markup.button.callback('+7', 'settings-utc-7'),
            ],
            [
                Markup.button.callback('+8', 'settings-utc-8'),
                Markup.button.callback('+9', 'settings-utc-9'),
                Markup.button.callback('+10', 'settings-utc-10'),
                Markup.button.callback('+11', 'settings-utc-11'),
                Markup.button.callback('+12', 'settings-utc-12'),
            ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-settings') ],
        ])
    ]
}

/** Actions */
mainScene.action(/settings-utc--?[0-9]{1,2}/, async ctx => {
    let value = +ctx.match[0].replace('settings-utc-', '')

    if (!value && value !== 0) return

    value = parseInt(value)

    if (value < -12 || value > 12) return

    const auth = await getAuth(ctx)

    auth.utc = value

    await auth.save()

    return ctx.editMessageText(...await settingsKeyboard(ctx))
})
