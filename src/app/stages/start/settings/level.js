/**
 * Level scene
 */
import { onInput } from '#scene'
import { roundTo } from '#utils/helpers.js'
import { getAuth } from '#controllers/userController.js'
import { settingsKeyboard } from './index.js'

/** Min and max values for level */
const MIN_LEVEL = 300
const MAX_LEVEL = 3000

/** Keyboard */
export async function levelKeyboard(ctx) {
    onInput(ctx, levelValue)

    return [ 'Введите свой максимум отработок за неделю.' ]
}

/** Messages */
export async function levelValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Значение не может быть пустым.')
    }

    value = +value.trim().replace(/\r?\n/g, '')

    if (isNaN(value) || value < MIN_LEVEL || value > MAX_LEVEL) {
        return ctx.reply(
            'Не верное значение уровня.\n' +
            `Необходимо ввести число от ${MIN_LEVEL} до ${MAX_LEVEL}`
        )
    }

    const auth = await getAuth(ctx)

    await auth.update({ level: roundTo(value) })

    onInput(ctx)

    return ctx.reply(...await settingsKeyboard(ctx))
}
