/**
 * Sweet edit
 */
import { onInput } from '#scene'
import { roundTo } from '#utils/helpers.js'
import { getAuth } from '#controllers/userController.js'
import { settingsKeyboard } from './index.js'

/** Min and max values for sweet */
const MIN_LEVEL = 50
const MAX_LEVEL = 1000

/** Keyboard */
export async function sweetKeyboard(ctx) {
    onInput(ctx, sweetValue)

    return [ 'Выберите размер надбавки за сладкое.' ]
}

/** Messages */
export async function sweetValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Значение не может быть пустым.')
    }

    value = +value.trim().replace(/\r?\n/g, '')

    if (isNaN(value) || value < MIN_LEVEL || value > MAX_LEVEL) {
        return ctx.reply(
            'Не верное значение для сладости.\n' +
            `Необходимо ввести число от ${MIN_LEVEL} до ${MAX_LEVEL}`
        )
    }

    const auth = await getAuth(ctx)

    await auth.update({ sweet_fine: roundTo(value) })

    onInput(ctx)

    return ctx.reply(...await settingsKeyboard(ctx))
}
