/**
 * Overeating edit
 */
import { onInput } from '#scene'
import { roundTo } from '#utils/helpers.js'
import { getAuth } from '#controllers/userController.js'
import { settingsKeyboard } from './index.js'

/** Min and max values for overeating */
const MIN_LEVEL = 100
const MAX_LEVEL = 3000

/** Keyboard */
export async function overeatingKeyboard(ctx) {
    onInput(ctx, overeatingValue)

    return [ 'Выберите надбавку за переедание.' ]
}

/** Actions */
export async function overeatingValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Значение не может быть пустым.')
    }

    value = +value.trim().replace(/\r?\n/g, '')

    if (isNaN(value) || value < MIN_LEVEL || value > MAX_LEVEL) {
        return ctx.reply(
            'Не верное значение для переедания.\n' +
            `Необходимо ввести число от ${MIN_LEVEL} до ${MAX_LEVEL}`
        )
    }

    const auth = await getAuth(ctx)

    await auth.update({ overeating_fine: roundTo(value) })

    onInput(ctx)

    return ctx.reply(...await settingsKeyboard(ctx))
}
