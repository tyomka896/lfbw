/**
 * Week penalty
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { settingsKeyboard } from './index.js'
import { getAuth } from '#controllers/userController.js'

/** Keyboard */
export async function weekKeyboard() {
    return [
        'Выберите размер штрафа:',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('25%', 'settings-week-25'),
                Markup.button.callback('50%', 'settings-week-50'),
                Markup.button.callback('75%', 'settings-week-75'),
            ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-settings') ],
        ])
    ]
}

/** Actions */
mainScene.action(/settings-week-(25|50|75)/, async ctx => {
    const value = +ctx.match[0].replace('settings-week-', '')

    if (!value) return

    const auth = await getAuth(ctx)

    auth.week_fine = value

    await auth.save()

    return ctx.editMessageText(...await settingsKeyboard(ctx))
})
