/**
 * Main start scene
 */
import { Scenes, Markup } from 'telegraf'
import { mainScene } from '#scene'
import { workoutKeyboard } from './workout/index.js'
import { penaltyKeyboard } from './penalty/index.js'
import { statisticKeyboard } from './statistic/index.js'
import { settingsKeyboard } from './settings/index.js'
import { getAuth } from '#controllers/userController.js'

/** Keyboard */
export async function startKeyboard(ctx) {
    const auth = await getAuth(ctx)

    return [
        'Главное меню',
        Markup.inlineKeyboard([
            [ Markup.button.callback(`🤸 Отработка`, 'start-workout', +auth.total_score <= 0) ],
            [ Markup.button.callback('🍽 Надбавка', 'start-penalty') ],
            [ Markup.button.callback('🗒 Статистика', 'start-statistic') ],
            [ Markup.button.callback('✏️ Настройки', 'start-settings') ],
        ])
    ]
}

/** Actions */
mainScene.action('start-workout', async ctx => {
    return ctx.editMessageText(...await workoutKeyboard(ctx))
})

mainScene.action('start-penalty', async ctx => {
    return ctx.editMessageText(...await penaltyKeyboard(ctx))
})

mainScene.action('start-statistic', async ctx => {
    return ctx.editMessageText(...await statisticKeyboard(ctx))
})

mainScene.action('start-settings', async ctx => {
    return ctx.editMessageText(...await settingsKeyboard(ctx))
})

mainScene.action('back-to-start', async ctx => {
    return ctx.editMessageText(...await startKeyboard(ctx))
})

export default new Scenes.Stage([ mainScene ])
