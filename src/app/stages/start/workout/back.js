/**
 * Back
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { SCORE_TYPES } from '#models/scoretype.js'
import { createScore } from '#controllers/scoreController.js'
import { workoutKeyboard } from './index.js'

/** Keyboard */
export function backKeyboard() {
    return [
        'Упражнений на спину:',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('-50', 'workout-back-50'),
                Markup.button.callback('-100', 'workout-back-100'),
                Markup.button.callback('-150', 'workout-back-150'),
            ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-workout') ],
        ])
    ]
}

/** Actions */
mainScene.action(/workout-back-(50|100|150)/, async ctx => {
    const value = +ctx.match[0].replace('workout-back-', '')

    if (!value) return

    const score = await createScore(ctx.from.id, SCORE_TYPES.BACK, value)

    if (!score) return

    const [ text, extra ] = await workoutKeyboard(ctx)

    return ctx.editMessageText([
            `_Спина: -${score.value}_\n`,
            `_Текущий счет: ${score.current_score}_\n\n`,
            text,
        ].join(''), {
            ...extra,
            parse_mode: 'Markdown',
        })
})
