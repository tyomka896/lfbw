/**
 * Abdominal
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { SCORE_TYPES } from '#models/scoretype.js'
import { createScore } from '#controllers/scoreController.js'
import { workoutKeyboard } from './index.js'

/** Keyboard */
export function abdominalKeyboard() {
    return [
        'Упражнений на пресс:',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('-50', 'workout-abdominal-50'),
                Markup.button.callback('-100', 'workout-abdominal-100'),
                Markup.button.callback('-150', 'workout-abdominal-150'),
            ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-workout') ],
        ])
    ]
}

/** Actions */
mainScene.action(/workout-abdominal-(50|100|150)/, async ctx => {
    const value = +ctx.match[0].replace('workout-abdominal-', '')

    if (!value) return

    const score = await createScore(ctx.from.id, SCORE_TYPES.ABDOMINAL, value)

    if (!score) return

    const [ text, extra ] = await workoutKeyboard(ctx)

    return ctx.editMessageText([
            `_Пресс: -${score.value}_\n`,
            `_Текущий счет: ${score.current_score}_\n\n`,
            text,
        ].join(''), {
            ...extra,
            parse_mode: 'Markdown',
        })
})
