/**
 * Cancel
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { workoutKeyboard } from './index.js'
import { deleteScore, getLastScore } from '#controllers/scoreController.js'

/** Keyboard */
export async function cancelKeyboard(ctx) {
    const score = await getLastScore(ctx.from.id, 'workout', false)
    const scoretype = await score.getScoretype()

    return [
        `Отменить -${score.value} ${scoretype.about}?`,
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Да', 'workout-cancel-yes'),
                Markup.button.callback('Нет', 'back-to-workout'),
            ],
        ])
    ]
}

/** Actions */
mainScene.action('workout-cancel-yes', async ctx => {
    const score = await getLastScore(ctx.from.id, 'workout', false)

    if (!score) return

    await deleteScore(score.id)

    const scoretype = await score.getScoretype()

    const [ text, extra ] = await workoutKeyboard(ctx)

    return ctx.editMessageText([
            `_-${score.value} ${scoretype.about} - отменены_\n\n`,
            text,
        ].join(''), {
            ...extra,
            parse_mode: 'Markdown',
        })
})
