/**
 * Pushup
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { SCORE_TYPES } from '#models/scoretype.js'
import { workoutKeyboard } from './index.js'
import { createScore } from '#controllers/scoreController.js'

/** Keyboard */
export function pushupKeyboard() {
    return [
        'Количество отжиманий:',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('-50', 'workout-pushup-50'),
                Markup.button.callback('-100', 'workout-pushup-100'),
                Markup.button.callback('-150', 'workout-pushup-150'),
            ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-workout') ],
        ])
    ]
}

/** Actions */
mainScene.action(/workout-pushup-(50|100|150)/, async ctx => {
    const value = +ctx.match[0].replace('workout-pushup-', '')

    if (!value) return

    const score = await createScore(ctx.from.id, SCORE_TYPES.PUSH_UP, value)

    if (!score) return

    const [ text, extra ] = await workoutKeyboard(ctx)

    return ctx.editMessageText([
            `_Отжимания: -${score.value}_\n`,
            `_Текущий счет: ${score.current_score}_\n\n`,
            text,
        ].join(''), {
            ...extra,
            parse_mode: 'Markdown',
        })
})
