/**
 * Workout
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { pushupKeyboard } from './pushup.js'
import { abdominalKeyboard } from './abdominal.js'
import { backKeyboard } from './back.js'
import { squatKeyboard } from './squat.js'
import { cancelKeyboard } from './cancel.js'
import { getLastScore } from '#controllers/scoreController.js'

/** Keyboard */
export async function workoutKeyboard(ctx) {
    const score = await getLastScore(ctx.from.id, 'workout')

    return [
        'Отработка',
        Markup.inlineKeyboard([
            [ Markup.button.callback('🦾 Отжимания', 'workout-pushup') ],
            [ Markup.button.callback('🥦 Пресс', 'workout-abdominal') ],
            [ Markup.button.callback('🍖 Спина', 'workout-back') ],
            [ Markup.button.callback('🦿 Приседания', 'workout-squat') ],
            [ Markup.button.callback('↪️ Отмена отработки', 'workout-cancel', !Boolean(score)) ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-start') ],
        ])
    ]
}

/** Actions */
mainScene.action('workout-pushup', async ctx => {
    return ctx.editMessageText(...pushupKeyboard())
})

mainScene.action('workout-abdominal', async ctx => {
    return ctx.editMessageText(...abdominalKeyboard())
})

mainScene.action('workout-back', async ctx => {
    return ctx.editMessageText(...backKeyboard())
})

mainScene.action('workout-squat', async ctx => {
    return ctx.editMessageText(...squatKeyboard())
})

mainScene.action('workout-cancel', async ctx => {
    if (!await getLastScore(ctx.from.id, 'workout')) {
        return ctx.editMessageText(...await workoutKeyboard(ctx))
    }

    return ctx.editMessageText(...await cancelKeyboard(ctx))
})

mainScene.action('back-to-workout', async ctx => {
    return ctx.editMessageText(...await workoutKeyboard(ctx))
})
