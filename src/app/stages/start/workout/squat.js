/**
 * Squat
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { SCORE_TYPES } from '#models/scoretype.js'
import { workoutKeyboard } from './index.js'
import { createScore } from '#controllers/scoreController.js'

/** Keyboard */
export function squatKeyboard() {
    return [
        'Количество приседаний:',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('-50', 'workout-squat-50'),
                Markup.button.callback('-100', 'workout-squat-100'),
                Markup.button.callback('-150', 'workout-squat-150'),
            ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-workout') ],
        ])
    ]
}

/** Actions */
mainScene.action(/workout-squat-(50|100|150)/, async ctx => {
    const value = +ctx.match[0].replace('workout-squat-', '')

    if (!value) return

    const score = await createScore(ctx.from.id, SCORE_TYPES.SQUAT, value)

    if (!score) return

    const [ text, extra ] = await workoutKeyboard(ctx)

    return ctx.editMessageText([
            `_Приседания: -${score.value}_\n`,
            `_Текущий счет: ${score.current_score}_\n\n`,
            text,
        ].join(''), {
            ...extra,
            parse_mode: 'Markdown',
        })
})
