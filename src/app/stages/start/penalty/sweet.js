/**
 * Sweet penalty
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { penaltyKeyboard } from './index.js'
import { SCORE_TYPES } from '#models/scoretype.js'
import { getAuth } from '#controllers/userController.js'
import { createScore } from '#controllers/scoreController.js'

/** Keyboard */
export async function sweetKeyboard(ctx) {
    const auth = await getAuth(ctx)

    const text = +auth.sweets > 0 ?
        'Будет снята одна бесплатная сладость!' :
        `Добавить +${auth.sweet_fine} за сладость?`

    return [
        text,
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Да', 'penalty-sweet-yes'),
                Markup.button.callback('Нет', 'back-to-penalty'),
            ],
        ])
    ]
}

/** Actions */
mainScene.action('penalty-sweet-yes', async ctx => {
    const auth = await getAuth(ctx)

    let score = null

    if (+auth.sweets > 0) {
        auth.sweets = +auth.sweets - 1

        await auth.save()
    }
    else {
        score = await createScore(auth.id, SCORE_TYPES.SWEET)

        if (!score) return
    }

    const totalScore = score ? score.current_score : auth.total_score

    const [ text, extra ] = await penaltyKeyboard(ctx)

    return ctx.editMessageText([
            score ?
                `_Сладость: +${score.value}_\n` :
                '_Снята бесплатная сладость_\n',
            `_Текущий счет: ${totalScore}_\n\n`,
            text,
        ].join(''), {
            ...extra,
            parse_mode: 'Markdown',
        })
})
