/**
 * Challenge penalty scene
 */
import { onInput } from '#scene'
import { SCORE_TYPES } from '#models/scoretype.js'
import { createScore } from '#controllers/scoreController.js'
import { penaltyKeyboard } from './index.js'

/** Min and Max values for challenge */
const MIN_LEVEL = 50
const MAX_LEVEL = 3000

/** Keyboard */
export async function challengeKeyboard(ctx) {
    onInput(ctx, penaltyValue)

    return [
        `Введите размер своего проигрыша.`,
    ]
}

/** Messages */
export async function penaltyValue(ctx) {
    let value = parseInt(ctx.message.text)

    if (!value || value < MIN_LEVEL || value > MAX_LEVEL) {
        return ctx.reply(`Не верное значение для спора. Попробуйте ввести число от ${MIN_LEVEL} до ${MAX_LEVEL}.`)
    }

    const score = await createScore(ctx.from.id, SCORE_TYPES.CHALLENGE, value)

    onInput(ctx)

    const [ text, extra ] = await penaltyKeyboard(ctx)

    return ctx.reply([
            `_Спор: +${score.value}_\n`,
            `_Текущий счет: ${score.current_score}_\n\n`,
            text,
        ].join(''), {
            ...extra,
            parse_mode: 'Markdown',
        })
}
