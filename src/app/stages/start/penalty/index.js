/**
 * Penalty
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { getLastScore } from '#controllers/scoreController.js'
import { sweetKeyboard } from './sweet.js'
import { overeatingKeyboard } from './overeating.js'
import { challengeKeyboard } from './challenge.js'
import { cancelKeyboard } from './cancel.js'

/** Keyboard */
export async function penaltyKeyboard(ctx) {
    const score = await getLastScore(ctx.from.id, 'penalty')

    return [
        'Надбавка',
        Markup.inlineKeyboard([
            [ Markup.button.callback(`🥞 Сладость`, 'penalty-sweet') ],
            [ Markup.button.callback(`🍱 Переедание`, 'penalty-overeating') ],
            [ Markup.button.callback(`🥊 На спор`, 'penalty-challenge') ],
            [ Markup.button.callback('↪️ Отмена надбавки', 'penalty-cancel', !Boolean(score)) ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-start'), ],
        ])
    ]
}

/** Actions */
mainScene.action('penalty-sweet', async ctx => {
    return ctx.editMessageText(...await sweetKeyboard(ctx))
})

mainScene.action('penalty-overeating', async ctx => {
    return ctx.editMessageText(...await overeatingKeyboard(ctx))
})

mainScene.action('penalty-challenge', async ctx => {
    await ctx.answerCbQuery()

    return ctx.reply(...await challengeKeyboard(ctx))
})

mainScene.action('penalty-cancel', async ctx => {
    if (! await getLastScore(ctx.from.id, 'penalty')) {
        return ctx.editMessageText(...await penaltyKeyboard(ctx))
    }

    return ctx.editMessageText(...await cancelKeyboard(ctx))
})

mainScene.action('back-to-penalty', async ctx => {
    return ctx.editMessageText(...await penaltyKeyboard(ctx))
})
