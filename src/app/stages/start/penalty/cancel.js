/**
 * Cancel penalty
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { deleteScore, getLastScore } from '#controllers/scoreController.js'
import { penaltyKeyboard } from './index.js'

/** Keyboard */
export async function cancelKeyboard(ctx) {
    const score = await getLastScore(ctx.from.id, 'penalty', false)
    const scoretype = await score.getScoretype()

    return [
        `Отменить +${score.value} ${scoretype.about}?`,
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Да', 'penalty-cancel-yes'),
                Markup.button.callback('Нет', 'back-to-penalty'),
            ],
        ])
    ]
}

/** Actions */
mainScene.action('penalty-cancel-yes', async ctx => {
    const score = await getLastScore(ctx.from.id, 'penalty', false)

    if (!score) return

    if (!await deleteScore(score.id)) {
        const [ text, extra ] = await penaltyKeyboard(ctx)

        return ctx.editMessageText([
                `_Не удалось отменить_❗️\n\n`,
                text,
            ].join(''), {
                ...extra,
                parse_mode: 'Markdown',
            })
    }

    const scoretype = await score.getScoretype()

    const [ text, extra ] = await penaltyKeyboard(ctx)

    return ctx.editMessageText([
            `_+${score.value} ${scoretype.about} - отменены_\n\n`,
            text,
        ].join(''), {
            ...extra,
            parse_mode: 'Markdown',
        })
})
