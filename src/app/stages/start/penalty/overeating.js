/**
 * Overeating penalty
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { penaltyKeyboard } from './index.js'
import { SCORE_TYPES } from '#models/scoretype.js'
import { getAuth } from '#controllers/userController.js'
import { createScore } from '#controllers/scoreController.js'

/** Keyboard */
export async function overeatingKeyboard(ctx) {
    const auth = await getAuth(ctx)

    return [
        `Добавить +${auth.overeating_fine} за переедание?`,
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Да', 'penalty-overeating-yes'),
                Markup.button.callback('Нет', 'back-to-penalty'),
            ],
        ])
    ]
}

/** Actions */
mainScene.action('penalty-overeating-yes', async ctx => {
    const score = await createScore(ctx.from.id, SCORE_TYPES.OVEREATING)

    if (!score) return

    const [ text, extra ] = await penaltyKeyboard(ctx)

    return ctx.editMessageText([
            `_Переедание: +${score.value}_\n`,
            `_Текущий счет: ${score.current_score}_\n\n`,
            text,
        ].join(''), {
            ...extra,
            parse_mode: 'Markdown',
        })
})
