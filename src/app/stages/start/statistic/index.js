/**
 * Statistic
 */
import { Markup } from 'telegraf'
import { mainScene } from '#scene'
import { lastActivity } from './activity.js'
import { basicStatus } from './basic.js'

/** Keyboard */
export async function statisticKeyboard(ctx) {
    return [
        'Статистика',
        Markup.inlineKeyboard([
            [ Markup.button.callback('📝 Статус', 'statistic-basic-status') ],
            [ Markup.button.callback('📖 История', 'statistic-activity') ],
            [ Markup.button.callback('⬅️ Назад', 'back-to-start') ],
        ])
    ]
}

/** Actions */
mainScene.action([
    'statistic-activity',
    /statistic-activity-page-[0-9]+/,
], lastActivity)

mainScene.action('statistic-basic-status', basicStatus)

mainScene.action('back-to-statistic', async ctx => {
    return ctx.editMessageText(...await statisticKeyboard())
})
