/**
 * Last user's activity
 */
import { Markup } from 'telegraf'
import dayjs from 'dayjs'
import 'dayjs/locale/ru.js'
import utc from 'dayjs/plugin/utc.js'

dayjs.locale('ru')
dayjs.extend(utc)

import { Score } from '#models/score.js'
import { SCORE_TYPES } from '#models/scoretype.js'
import { getAuth } from '#controllers/userController.js'

/** Amount of activity rows */
const SCORES_PER_PAGE = 15

/**
 * Get current page of activity list
 */
function getActivityPage(action) {
    if (!action.includes('statistic-activity-page-')) return 0

    return +action.replace('statistic-activity-page-', '')
}

export async function lastActivity(ctx) {
    const auth = await getAuth(ctx)

    const scoresCount = await Score.count({ where: { user_id: auth.id } })

    if (scoresCount === 0) {
        return ctx.reply('Ни одной активности не найдено!')
    }

    let page = getActivityPage(ctx.match[0])
    let offset = page * SCORES_PER_PAGE

    if (scoresCount <= offset) page = offset = 0

    const scores = await Score.findAll({
        where: { user_id: auth.id },
        order: [ [ 'created_at', 'desc' ] ],
        include: [ 'Scoretype' ],
        offset: offset,
        limit: SCORES_PER_PAGE,
    })

    const rows = [
        `_История действий (${page + 1}/${Math.ceil(scoresCount / SCORES_PER_PAGE)})_\n\n`,
    ]

    const { PUSH_UP, ABDOMINAL, BACK, SQUAT } = SCORE_TYPES

    let sign = ''

    for (let score of scores) {
        sign = '+'

        const createdAt = dayjs(score.created_at)
            .utcOffset(auth.utc)
            .format('D MMM HH:mm:ss')

        if ([ PUSH_UP, ABDOMINAL, BACK, SQUAT ].includes(score.type_id) ||
            score.value < 0) {
            sign = '-'
        }

        rows.push(`_${createdAt}_: *${sign}${score.value}* ${score.Scoretype.about}\n`)
    }

    return ctx.editMessageText(rows.join(''), {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback('◀️', `statistic-activity-page-${page - 1}`,
                        page === 0),
                    Markup.button.callback('▶️', `statistic-activity-page-${page + 1}`,
                        SCORES_PER_PAGE >= scoresCount - offset),
                ],
                [ Markup.button.callback('⬅️ Назад', 'back-to-statistic'), ]
            ]),
            parse_mode: 'Markdown',
        })
}
