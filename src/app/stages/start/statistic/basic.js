/**
 * Basic user's information
 */
import { Markup } from 'telegraf'
import { sequelize } from '#connection'

const TOTAL_USERS_SCORE =
`select type_id, SUM(value) as total
from score
where user_id=?
group by user_id, type_id;`

export async function basicStatus(ctx) {
    const [ result ] = await sequelize.query(TOTAL_USERS_SCORE, {
        replacements: [ ctx.from.id ],
    })

    const status = result.reduce((red, elem) => {
        red[elem.type_id] = elem.total

        return red
    }, {})

    return ctx.editMessageText([
            '_Общий статус_\n\n',
            `*${status.R || '🚫'}* : бесплатных 🎂\n\n`,
            `*${status.P || '🚫'}* : отжимания 🦾\n`,
            `*${status.A || '🚫'}* : пресс 🥦\n`,
            `*${status.B || '🚫'}* : спина 🍖\n`,
            `*${status.Q || '🚫'}* : приседания 🦿\n\n`,
            `*${status.S || '🚫'}* : сладкое 🥞\n`,
            `*${status.O || '🚫'}* : переедания 🍱\n`,
            `*${status.C || '🚫'}* : на спор 🥊\n`,
            `*${status.F || '🚫'}* : штрафа 🔫\n`,
        ].join(''), {
            ...Markup.inlineKeyboard([
                [ Markup.button.callback('⬅️ Назад', 'back-to-statistic') ],
            ]),
            parse_mode: 'Markdown',
        })
}
