/**
 * Main stage
 */
import { Scenes } from 'telegraf'
import { mainScene } from '#scene'
import { random } from '#utils/random.js'
import help from '../commands/help.js'
import { startKeyboard } from './start/index.js'

/** Actions */
mainScene.enter(async ctx => {
    let text, extra

    switch (ctx.scene.state.act) {
        case 'start': [ text, extra ] = await startKeyboard(ctx); break
        default: return help(ctx)
    }

    return ctx.reply(text, extra)
})

/**
 * User's input handler
 */
mainScene.hears(/^([^\/]|\/empty).*/, async ctx => {
    if (ctx.session.input) {
        if (ctx.message.text === '/empty') {
            ctx.message.text = null
        }

        const { path, name } =  ctx.session.input

        const module = await import(path)

        if (module[name]) {
            return module[name](ctx)
        }
    }

    if (ctx.message.text.length === 2) {
        const unicode = ctx.message.text.codePointAt(0).toString(16)

        if (unicode.substring(0, 2) === '1f') {
            const emojis = [
                '😁', '🤣', '😇', '🙃', '🥰', '😋',
                '🤪', '🤓', '🤩', '🥳', '😏', '😳',
                '🤗', '🤔', '🤫', '😬', '🙄', '😴',
                '🤮', '🤠', '💩', '👻', '🤘', '🐣',
            ]

            return ctx.reply(random(emojis))
        }
    }

    return ctx.reply('Отправь /help для вывода полного списка команд.')
})

/**
 * Delete message
 */
mainScene.action('delete-message', async ctx => {
    await ctx.answerCbQuery()

    await ctx.deleteMessage()
        .catch(() => {
            const { message_id } = ctx.update?.callback_query?.message

            const extra = message_id ?
                { reply_to_message_id: message_id } :
                null

            ctx.reply(
                'Не удалось удалить сообщение, удалите его вручную.',
                extra
            )
        })
})

export const stage = new Scenes.Stage([ mainScene ])
