/**
 * User last visit
 */
import { sequelize } from '#connection'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

export default async function(ctx, next) {
    if (ctx.from) {
        await sequelize
            .query('UPDATE "user" SET "last_visit"=? WHERE "id"=?', {
                replacements: [
                    dayjs().toDate(),
                    ctx.from.id,
                ]
            })
    }

    return next()
}
