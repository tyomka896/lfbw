/**
 * User model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'
import { Score } from '#models/score.js'
import { Session } from '#models/session.js'

/**
 * Extending model
 */
export class User extends Model {
    /**
     * Control user's notification settings
     */
    notify(obj) {
        if (typeof obj !== 'string' && typeof obj !== 'object') return null

        let json = {}

        try {
            json = JSON.parse(this._notify)
        }
        catch (error) {}

        json = Object.assign({
            fine: true,
            free: true,
            quote: false,
        }, json)

        if (typeof obj === 'string') {
            return json[obj] || null
        }
        else if (typeof obj === 'object') {
            const keys = Object.keys(obj)

            if (keys.length === 0) return null

            for (let key of keys) {
                json[key] = obj[key]
            }

            this._notify = JSON.stringify(json)

            this.save()
        }

        return this
    }

    /**
     * If the user is admin
     */
    admin() {
        return this.role === 'admin'
    }

    /**
     * Link name from login or first and last names
     */
    linkName() {
        if (this.username) return `@${this.username}`

        return [
            this.last_name || '',
            this.first_name
        ]
            .filter(elem => elem)
            .join(' ')
    }

    /**
     * Concat last name, first name and login
     */
    fullName() {
        return [
            this.last_name || '',
            this.first_name,
            this.username ? `@${this.username}` : ''
        ]
            .filter(elem => elem)
            .join(' ')
    }
}

/**
 * Model structure
 */
User.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    utc: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
    },
    role: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: 'user',
    },
    username: {
        type: DataTypes.TEXT,
    },
    first_name: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    last_name: {
        type: DataTypes.TEXT,
    },
    _notify: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '{}',
        field: 'notify',
    },
    total_score: {
        type: DataTypes.NUMBER,
        allowNull: false,
        defaultValue: 0,
    },
    sweets: {
        type: DataTypes.NUMBER,
        allowNull: false,
        defaultValue: 0,
    },
    sweet_score_id: {
        type: DataTypes.NUMBER,
        allowNull: false,
        defaultValue: 0,
    },
    level: {
        type: DataTypes.NUMBER,
        allowNull: false,
        defaultValue: 300,
    },
    sweet_fine: {
        type: DataTypes.NUMBER,
        allowNull: false,
        defaultValue: 50,
    },
    overeating_fine: {
        type: DataTypes.NUMBER,
        allowNull: false,
        defaultValue: 100,
    },
    week_fine: {
        type: DataTypes.NUMBER,
        allowNull: false,
        defaultValue: 25,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    last_visit: {
        type: DataTypes.DATE,
        allowNull: false,
    }
}, {
    sequelize,
    tableName: 'user',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})

/**
 * Get the scores for the user.
 */
User.hasMany(Score, {
    foreignKey: 'user_id',
})

/**
 * Get the user that owns the score.
 */
Score.belongsTo(User, {
    foreignKey: 'user_id'
})

/**
 * Get the sessions for the user.
 */
User.hasMany(Session, {
    foreignKey: 'user_id',
})
