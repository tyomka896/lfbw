/**
 * Score model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'
import { Scoretype } from '#models/scoretype.js'

/**
 * Extending model
 */
export class Score extends Model {  }

/**
 * Model structure
 */
Score.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    type_id: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    value: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    current_score: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'score',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: false,
})


/**
 * Get the scoretype that owns the score.
 */
Score.belongsTo(Scoretype, {
    foreignKey: 'type_id'
})

/**
 * Get the scores for the scoretype.
 */
Scoretype.hasMany(Score, {
    foreignKey: 'type_id',
    as: 'Scores'
})
