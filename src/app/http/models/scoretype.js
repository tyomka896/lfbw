/**
 * Scoretype model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/** Score type enum */
export const SCORE_TYPES = {
    PUSH_UP: 'P',
    ABDOMINAL: 'A',
    BACK: 'B',
    SQUAT: 'Q',
    SWEET: 'S',
    OVEREATING: 'O',
    FINE: 'F',
    FREE_SWEET: 'R',
    CHALLENGE: 'C',
}

/**
 * Extending model
 */
export class Scoretype extends Model {  }

/**
 * Model structure
 */
Scoretype.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    name: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    about: {
        type: DataTypes.TEXT,
    },
}, {
    sequelize,
    tableName: 'scoretype',
    timestamps: false,
})
