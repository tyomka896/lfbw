/**
 * Score controller
 */
import { Op } from 'sequelize'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

import { User } from '#models/user.js'
import { Score } from '#models/score.js'
import { roundTo } from '#utils/helpers.js'

/**
 * Create any score
 * @param {Number} userId
 * @param {Number} typeId
 * @param {Number} value
 */
export async function createScore(userId, typeId, value) {
    const user = await User.findByPk(userId)

    if (!user) return null

    typeId = typeId.toString().toUpperCase()

    switch (typeId) {
        case 'S': value = user.sweet_fine; break
        case 'O': value = user.overeating_fine; break
    }

    value = parseInt(value)

    if (value <= 0) return null

    if (value % 50 !== 0) value = roundTo(value)

    const totalScore = +user.total_score

    if ([ 'P', 'A', 'B', 'Q' ].includes(typeId)) {
        if (totalScore <= 0) return null

        value = value > totalScore ? totalScore : value

        user.total_score = totalScore - value
    }
    else if ([ 'S', 'O', 'C', 'F' ].includes(typeId)) {
        user.total_score = totalScore + value
    }
    else {
        return null
    }

    await user.save()

    return await Score.create({
        user_id: user.id,
        type_id: typeId,
        value: value,
        current_score: user.total_score,
    })
}

/**
 * Delete model
 * @param {Numeric} scoreId
 */
export async function deleteScore(scoreId) {
    const score = await Score.findByPk(scoreId)

    if (!score) return false

    const user = await score.getUser()

    const totalScore = +user.total_score

    if ([ 'P', 'A', 'B', 'Q' ].includes(score.type_id)) {
        user.total_score = totalScore + +score.value
    }
    else if ([ 'S', 'O', 'C' ].includes(score.type_id)) {
        if (totalScore < +score.value) return false

        user.total_score = totalScore - +score.value
    }

    await score.destroy()

    await user.save()

    return true
}

/**
 * Get last model
 * @param {Numeric} userId
 * @param {String} type - score types workout/penalty group
 * @param {Boolean} limit - created less than 15 minutes ago
 */
export async function getLastScore(userId, type, limit = true) {
    const user = await User.findByPk(userId)

    if (!user) return null

    let typesIn = null

    switch (type) {
        case 'workout': typesIn = [ 'P', 'A', 'B', 'Q' ]; break
        case 'penalty': typesIn = [ 'S', 'O', 'C' ]; break
    }

    if (!typesIn) return null

    const score = await Score.findOne({
        where: {
            [Op.and]: [
                { user_id: userId },
                { type_id: { [Op.in]: typesIn } },
            ],
        },
        order: [ [ 'created_at', 'desc' ] ],
    })

    if (!score) return null

    if (limit && dayjs.utc().diff(score.created_at, 'minute') >= 15) return null

    return score
}
