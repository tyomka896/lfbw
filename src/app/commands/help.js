/**
 * Help command
 */
import { getAuth } from '#controllers/userController.js'

export default async function(ctx) {
    const auth = await getAuth(ctx)

    const rows = [
        '*Команды*\n',
        '/start - начать диалог\n',
        '/balance - текущий баланс\n',
        '/free - когда бесплатное сладкое\n',
        '/cancel - отмена последнего действия\n',
    ]

    if (auth.admin()) {
        rows.push('\n\n*Управление*\n')
        rows.push('`/add\_sweets` - добавить сладостей\n')
        rows.push('/users\\_list - список пользователей')
    }

    return ctx.reply(rows.join(''), { parse_mode: 'Markdown' })
}
