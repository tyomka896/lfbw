/**
 * List of all users with info
 */
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

import { User } from '#models/user.js'
import { getAuth } from '#controllers/userController.js'

export default async function(ctx) {
    const auth = await getAuth(ctx)

    if (!auth.admin()) return

    const users = await User.findAll()

    const rows = []

    for (let user of users) {
        rows.push(`<code>#${user.id}</code> <b>${user.fullName()}</b>\n`)

        const scores = await user.getScores()

        const lastVisit = dayjs(user.last_visit)
            .utcOffset(auth.utc)
            .format('DD.MM.YYYY HH:mm:ss')

        rows.push(`<i>${user.total_score} (${scores.length}) - ${lastVisit}</i>\n`)
    }

    return ctx.replyWithHTML(rows.join(''), { parse_mode: 'HTML' })
}
