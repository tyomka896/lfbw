/**
 * Start the main scene
 */
export default async function(ctx) {
    return ctx.scene.enter('main', { act: 'start' } )
}
