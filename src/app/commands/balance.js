/**
 * Current user's balance
 */
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

import { fineByLevel, calcWeekFine } from '#utils/helpers.js'
import { getAuth } from '#controllers/userController.js'

/**
 * Total score icon based on score value
 */
function getScoreIcon(value) {
    if (value >= 5000) return '☠️'
    else if (value >= 3000) return '💩'
    else if (value >= 2500) return '🤡'
    else if (value >= 2000) return '🤯'
    else if (value >= 1500) return '😱'
    else if (value >= 1000) return '😭'
    else if (value >= 600) return '😔'
    else if (value >= 300) return '😯'
    else if (value > 0) return '😏'
    else return '🦾'
}

/**
 * Sweet icon based on free sweets
 */
function getSweetIcon(value) {
    if (value > 6) return '🎂'

    switch(value) {
        case 0: return '🚫'
        case 1: return '🍬'
        case 2: return '🍫'
        case 3: return '🍩'
        case 4: return '🍦'
        case 5: return '🍮'
        case 6: return '🍰'
    }
}

export default async function(ctx) {
    const auth = await getAuth(ctx)

    const scoreIcon = getScoreIcon(+auth.total_score)
    const sweetIcon = getSweetIcon(+auth.sweets)

    const rows = [
        `${scoreIcon} Текущий счет ${auth.total_score}`,
        `${sweetIcon} Бесплатных сладостей ${auth.sweets}`,
    ]

    const weekDay = dayjs().utcOffset(auth.utc).day()

    if (+auth.total_score !== 0 &&
        (weekDay === 6 || weekDay === 0))
    {
        const maxFine = fineByLevel(auth.level, auth.week_fine)
        let finedScore = calcWeekFine(auth.total_score, auth.week_fine)

        if (finedScore > maxFine) finedScore = maxFine

        rows.push(`🤭 В Пн. счет будет ${finedScore}`)
    }

    return ctx.reply(rows.join('\n'))
}
