/**
 * Add sweets to user
 *
 * - /add_sweets {id} {value}
 */
import { User } from '#models/user.js'
import { Score } from '#models/score.js'
import { SCORE_TYPES } from '#models/scoretype.js'
import { numberToWords } from '#utils/helpers.js'
import { getAuth } from '#controllers/userController.js'

export default async function(ctx) {
    const auth = await getAuth(ctx)

    if (!auth.admin()) return

    const parts = ctx.message.text.split(' ')

    if (parts.length < 3) {
        return ctx.reply('Запрос построен не верно!')
    }

    const userId = parts[1].charAt(0) === '#' ? parts[1].substring(1) : parts[1]

    const user = await User.findByPk(userId)

    if (!user) {
        return ctx.reply(`Пользователь с #${userId} не найден!`)
    }

    const value = parseInt(parts[2])

    if (!value) {
        return ctx.reply(`Значение сладкого задано не верно!`)
    }
    else if (value < 1 || value > 10) {
        return ctx.reply(`Значение сладкого должно быть в диапазоне от 1 до 10!`)
    }

    user.sweets = +user.sweets + value

    await user.save()

    await Score.create({
        user_id: user.id,
        type_id: SCORE_TYPES.FREE_SWEET,
        value: value,
        current_score: user.total_score,
    })

    const sweetWords =
        numberToWords(value, 'бесплатное_бесплатных_бесплатных') + ' ' +
        numberToWords(value, 'сладкое_сладости_сладости')

    if (user.id !== ctx.from.id) {
        await ctx.reply(`Пользователю '${user.fullName()}' добавлено +${value} ${sweetWords}`)
    }

    if (Boolean(user.notify('free'))) {
        await ctx.telegram.sendMessage(user.id, `Вам подарено +${value} ${sweetWords} 😋`)
    }
}
