/**
 * Time before free sweets
 */
import { Op } from 'sequelize'
import dayjs from 'dayjs'
import 'dayjs/locale/ru.js'
import utc from 'dayjs/plugin/utc.js'
import duration from 'dayjs/plugin/duration.js'

dayjs.locale('ru')
dayjs.extend(utc)
dayjs.extend(duration)

import { Score } from '#models/score.js'
import { SCORE_TYPES } from '#models/scoretype.js'
import { numberToWords } from '#utils/helpers.js'
import { getAuth } from '#controllers/userController.js'

/**
 * Number to word
 */
function durationToWords(duration) {
    const time = []

    if (duration.days() > 0) {
        time.push(
            duration.days() + ' ' +
            numberToWords(duration.days(), 'день_дня_дней')
        )
    }

    if (duration.hours() > 0) {
        time.push(
            duration.hours() + ' ' +
            numberToWords(duration.hours(), 'час_часа_часов')
        )
    }

    if (duration.minutes() > 0) {
        time.push(
            duration.minutes() + ' ' +
            numberToWords(duration.minutes(), 'минуту_минуты_минут') + ' и'
        )
    }

    time.push(
        duration.seconds() + ' ' +
        numberToWords(duration.seconds(), 'секунда_секунды_секунд')
    )

    return time.join(' ')
}

export default async function(ctx) {
    const auth = await getAuth(ctx)

    const score = await Score.findOne({
        where: {
            user_id: auth.id,
            type_id: { [Op.not]: SCORE_TYPES.FREE_SWEET }
        },
        order: [ [ 'created_at', 'desc' ] ],
    })

    if (!score) {
        return ctx.reply('Ни одной активности не найдено!')
    }
    else if (+auth.total_score > 0 || +score.current_score > 0) {
        return ctx.reply('Отсчет времени до бесплатного сладкого возможен только при нулевом счете 😕')
    }
    else if (auth.sweet_score_id === score.id) {
        const sweetAddedAt = dayjs(score.created_at)
            .utcOffset(auth.utc)
            .add(7, 'day')
            .format('D MMMM в HH:mm:ss')

        return ctx.reply(`Вы уже получили бесплатную сладость от ${sweetAddedAt} 😋`)
    }

    const createdAt = dayjs(score.created_at).add(7, 'days')

    if (dayjs().isBefore(createdAt)) {
        const difference = createdAt.diff(dayjs())

        const duration = dayjs.duration(difference)

        return ctx.reply(`Бесплатная сладость через ${durationToWords(duration)} ⏱`)
    }

    await Score.create({
        user_id: auth.id,
        type_id: SCORE_TYPES.FREE_SWEET,
        value: 1,
        current_score: auth.total_score,
        created_at: dayjs(score.created_at).add(7, 'day')
    })

    auth.sweets = +auth.sweets + 1
    auth.sweet_score_id = score.id

    await auth.save()

    return ctx.reply(`Вам начислена бесплатная сладость 🍰`)
}
