/**
 * Helpers
 */

/**
 * Parse value to boolean
 * @param {any} value
 */
export function toBoolean(value) {
    return value?.toString()?.toLowerCase?.() === 'true' ||
        Boolean(parseInt(value))
}

/**
 * Round the number to specified significance
 * @param {Number} number
 * @param {Number} step
 */
export function roundTo(number, step = 50) {
    return Math.ceil(+number / +step) * +step
}

/**
 * Maximum fine per week
 * @param {Number} level - user's fine per week
 * @param {Number} weekFine - amount of scores he is ready to workout per week
 */
export function fineByLevel(level, weekFine) {
    const value = +level * (100 + +weekFine) / +weekFine - +level * 0.8

    return roundTo(value)
}

/**
 * Score result with week fine
 * @param {Number} totalScore - current score of a user
 * @param {Number} weekFine - user's fine per week
 */
export function calcWeekFine(totalScore, weekFine) {
    const value = +totalScore * (1 + +weekFine / 100)

    return roundTo(value)
}

/**
 * Number to words translator
 * @param {Number} value - number
 * @param {Array} words - array od declinations
 */
export function numberToWords(value, words) {
    const cases = [2, 0, 1, 1, 1, 2] // ru

    value = Math.abs(+value)

    if (typeof words === 'string') {
        words = words.split('_')
    }
    else if (! Array.isArray(words)) {
        throw Error('Invalid parameter: words.')
    }

    return words[(value % 100 > 4 && value % 100 < 20) ? 2 :
        cases[(value % 10 < 5) ? value % 10 : 5]]
}
