/**
 * App configuration
 */
import 'dotenv/config'
import path from 'path'
import { toBoolean } from '#utils/helpers.js'

const env = {
    BOT_TOKEN: process.env.BOT_TOKEN,
    BOT_SHUTDOWN: toBoolean(process.env.BOT_SHUTDOWN),

    LOGGING: toBoolean(process.env.LOGGING),
    DB_LOGGING: toBoolean(process.env.DB_LOGGING),
}

global.env = (key, def = null) => env[key] !== undefined ? env[key] : def

global.storagePath = () => path.resolve('src/storage')
