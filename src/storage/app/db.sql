--
-- File generated with SQLiteStudio v3.3.3 on Пн июл 4 20:53:10 2022
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: score
DROP TABLE IF EXISTS score;

CREATE TABLE score (
    id            INTEGER PRIMARY KEY
                          UNIQUE
                          NOT NULL,
    user_id       INTEGER NOT NULL
                          CONSTRAINT score_user_id REFERENCES user (id) ON DELETE CASCADE
                                                                        ON UPDATE CASCADE,
    type_id       TEXT    NOT NULL
                          CONSTRAINT score_type_id REFERENCES scoretype (id) ON UPDATE CASCADE,
    value         INTEGER NOT NULL,
    current_score INTEGER NOT NULL,
    created_at    TEXT    NOT NULL
);


-- Table: scoretype
DROP TABLE IF EXISTS scoretype;

CREATE TABLE scoretype (
    id    TEXT PRIMARY KEY
               UNIQUE
               NOT NULL,
    name  TEXT NOT NULL,
    about TEXT
);


INSERT INTO scoretype (id, name, about)
VALUES
    ('P', 'PUSH-UP', 'отжимания'),
    ('A', 'ABDOMINAL', 'пресс'),
    ('B', 'BACK', 'спина'),
    ('Q', 'SQUAT', 'приседания'),
    ('S', 'SWEET', 'сладкое'),
    ('O', 'OVEREATING', 'переедание'),
    ('F', 'FINE', 'штраф'),
    ('R', 'FREE_SWEET', 'бесплатное слаткое'),
    ('C', 'CHALLENGE', 'спор');


-- Table: session
DROP TABLE IF EXISTS session;

CREATE TABLE session (
    user_id    INTEGER NOT NULL
                       CONSTRAINT session_user_id REFERENCES user (id) ON DELETE CASCADE
                                                                       ON UPDATE CASCADE,
    chat_id    INTEGER NOT NULL,
    value      TEXT    NOT NULL
                       DEFAULT ('{}'),
    created_at TEXT    NOT NULL,
    updated_at TEXT    NOT NULL,
    CONSTRAINT session_user_chat_id PRIMARY KEY (
        user_id,
        chat_id
    )
);


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    id              INTEGER PRIMARY KEY
                            UNIQUE
                            NOT NULL,
    utc             INTEGER NOT NULL
                            DEFAULT (0),
    role            TEXT    NOT NULL
                            DEFAULT user,
    username        TEXT    UNIQUE,
    first_name      TEXT    NOT NULL,
    last_name       TEXT,
    notify          TEXT    NOT NULL
                            DEFAULT ('{}'),
    total_score     INTEGER NOT NULL
                            DEFAULT (0),
    sweets          INTEGER NOT NULL
                            DEFAULT (0),
    sweet_score_id  INTEGER NOT NULL
                            DEFAULT (0),
    level           INTEGER NOT NULL
                            DEFAULT (300),
    sweet_fine      INTEGER NOT NULL
                            DEFAULT (50),
    overeating_fine INTEGER NOT NULL
                            DEFAULT (100),
    week_fine       INTEGER NOT NULL
                            DEFAULT (25),
    created_at      TEXT    NOT NULL,
    updated_at      TEXT    NOT NULL,
    last_visit      TEXT    NOT NULL
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
